package com.session4datadriven;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class PassingMultipleDataRegister {

	public static void main(String[] args) throws BiffException, IOException {
		
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demowebshop.tricentis.com/");
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
		
		File f1=new File("/home/dharmaraj/Documents/Sample/testdata1.xls");
		FileInputStream fis=new FileInputStream(f1);
		Workbook wb=Workbook.getWorkbook(fis);
		Sheet sh=wb.getSheet("RegisterData");
		int rows=sh.getRows();
		int columns=sh.getColumns();
		for (int i = 0; i < rows; i++) {
			String firstname=sh.getCell(0,1).getContents();
			String lastname=sh.getCell(1,1).getContents();
			String email=sh.getCell(2,1).getContents();
			String password=sh.getCell(3,1).getContents();
			
			driver.findElement(By.xpath("//a[text()='Register']")).click();
			driver.findElement(By.id("gender-male")).click();
			driver.findElement(By.id("FirstName")).sendKeys(firstname);
			driver.findElement(By.id("LastName")).sendKeys(lastname);
			driver.findElement(By.id("Email")).sendKeys(email);
			driver.findElement(By.id("Password")).sendKeys(password);
			driver.findElement(By.id("ConfirmPassword")).sendKeys(password);
			driver.findElement(By.xpath("//input[@value='Register']")).click();
			driver.findElement(By.linkText("Log out")).click();
		}

	}

}
