package com.session4datadriven;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class PassingMultipleDatatoRegisterDataUsingApache {

	public static void main(String[] args) throws IOException {
		
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demowebshop.tricentis.com/");
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
		
		File f1=new File("/home/dharmaraj/Documents/Sample/testdata2.xlsx");
		FileInputStream fis=new FileInputStream(f1);
		XSSFWorkbook xwb= new XSSFWorkbook(fis);
		XSSFSheet sh=xwb.getSheetAt(1);
		int rows=sh.getPhysicalNumberOfRows();
		for (int i = 1; i < rows; i++) {
			String firstname=sh.getRow(i).getCell(0).getStringCellValue();
			String lastname=sh.getRow(i).getCell(1).getStringCellValue();
			String email=sh.getRow(i).getCell(2).getStringCellValue();
			String password=sh.getRow(i).getCell(3).getStringCellValue();
			driver.findElement(By.xpath("//a[text()='Register']")).click();
			driver.findElement(By.id("gender-male")).click();
			driver.findElement(By.id("FirstName")).sendKeys(firstname);
			driver.findElement(By.id("LastName")).sendKeys(lastname);
			driver.findElement(By.id("Email")).sendKeys(email);
			driver.findElement(By.id("Password")).sendKeys(password);
			driver.findElement(By.id("ConfirmPassword")).sendKeys(password);
			driver.findElement(By.xpath("//input[@value='Register']")).click();
			driver.findElement(By.linkText("Log out")).click();
		}

	}

}
