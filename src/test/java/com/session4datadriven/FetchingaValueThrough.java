package com.session4datadriven;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class FetchingaValueThrough {

	public static void main(String[] args) throws BiffException, IOException {
		
		File f1=new File("/home/dharmaraj/Documents/Sample/testdata1.xls");
		FileInputStream fis=new FileInputStream(f1);
		Workbook wb=Workbook.getWorkbook(fis);
		Sheet sh=wb.getSheet("Sheet1");
		String celldata=sh.getCell(1,2).getContents();
		System.out.println("value present is: "+celldata);
		
		int rows=sh.getRows();
		int columns=sh.getColumns();
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				celldata=sh.getCell(j,i).getContents();
				System.out.println("Row and Column present is: "+celldata);
			}
			
		}

	}

}
