package com.session4datadriven;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {
	
	public static String[][] getExcelData() throws IOException{
		File f1=new File("/home/dharmaraj/Documents/Sample/testdata2.xlsx");
		FileInputStream fis=new FileInputStream(f1);
		XSSFWorkbook xwb= new XSSFWorkbook(fis);
		XSSFSheet sh=xwb.getSheetAt(0);
		int rows=sh.getPhysicalNumberOfRows();
		String[][] data= new String[2][1];
		for (int i = 1; i < rows; i++) {
			int columns=sh.getRow(i).getLastCellNum();
			for (int j = 0; j < columns; j++) {
				String cellvalue=sh.getRow(i).getCell(j).getStringCellValue();
//				System.out.println(cellvalue);
				data[i-1][j]=cellvalue;
			}
			
		}
		return data;
	}

}
