package com.session4datadriven;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class PassingMultipleData {

	public static void main(String[] args) throws BiffException, IOException {
		
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demowebshop.tricentis.com/");
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
		
		File f1=new File("/home/dharmaraj/Documents/Sample/testdata1.xls");
		FileInputStream fis=new FileInputStream(f1);
		Workbook wb=Workbook.getWorkbook(fis);
		Sheet sh=wb.getSheet("Sheet1");
//		String celldata=sh.getCell(1,2).getContents();
//		System.out.println("value present is: "+celldata);
		
		int rows=sh.getRows();
		int columns=sh.getColumns();
		for (int i = 0; i < rows; i++) {
			String username=sh.getCell(0,1).getContents();
			String password=sh.getCell(1,1).getContents();
			
			
		driver.findElement(By.xpath("//a[text()='Log in']")).click();
		driver.findElement(By.id("Email")).sendKeys(username);
		driver.findElement(By.id("Password")).sendKeys(password);
		driver.findElement(By.xpath("//input[@value='Log in']")).click();
		
		driver.findElement(By.linkText("Log out")).click();
		
		}

	}

}
