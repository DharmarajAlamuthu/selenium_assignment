package com.session4datadriven;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import jxl.Workbook;

public class FetchingDataThroughApach {

	public static void main(String[] args) throws IOException {
		
		File f1=new File("/home/dharmaraj/Documents/Sample/testdata2.xlsx");
		FileInputStream fis=new FileInputStream(f1);
		XSSFWorkbook xwb= new XSSFWorkbook(fis);
		XSSFSheet sh=xwb.getSheetAt(0);
		int rows=sh.getPhysicalNumberOfRows();
		for (int i = 0; i < rows; i++) {
			int columns=sh.getRow(i).getLastCellNum();
			for (int j = 0; j < columns; j++) {
				String cellvalue=sh.getRow(i).getCell(j).getStringCellValue();
				System.out.println(cellvalue);
			}
			
		}

	}

}
