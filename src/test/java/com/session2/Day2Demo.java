package com.session2;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Day2Demo {

	public static void main(String[] args) {
		
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://demowebshop.tricentis.com/");
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
		
		driver.findElement(By.xpath("//a[text()='Log in']")).click();
		driver.findElement(By.id("Email")).sendKeys("manzmehadi1@gmail.com");
		driver.findElement(By.id("Password")).sendKeys("Mehek@110");
		driver.findElement(By.xpath("//input[@value='Log in']")).click();
		
		String str="Demo Web Shop";
		String title = driver.getTitle();
		if(title.equals(str)) {
			System.out.println("Pass");
		}
		else {
			System.out.println("Fail");
		}
		driver.quit();

	}

}
